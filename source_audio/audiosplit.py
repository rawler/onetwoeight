# Import the AudioSegment class for processing audio and the
# split_on_silence function for separating out silent chunks.
from pydub import AudioSegment
from pydub.silence import split_on_silence
from sys import argv

# Define a function to normalize a chunk to a target amplitude.
def match_target_amplitude(aChunk, target_dBFS):
    ''' Normalize given audio chunk '''
    change_in_dBFS = target_dBFS - aChunk.dBFS
    return aChunk.apply_gain(change_in_dBFS)

# Load your audio.
song = AudioSegment.from_file(argv[1]+".wav")

# Split track where the silence is 2 seconds or more and get chunks using
# the imported function.
chunks = split_on_silence (
    # Use the loaded audio.
    song,
    # Specify that a silent chunk must be at least 2 seconds or 2000 ms long.
    min_silence_len = 600,
    # Consider a chunk silent if it's quieter than -16 dBFS.
    # (You may want to adjust this parameter.)
    silence_thresh = -36
)

# Process each chunk with your parameters
for i, chunk in enumerate(chunks):
    # Normalize the entire chunk.
    # normalized_chunk = match_target_amplitude(audio_chunk, -20.0)

    # Export the audio chunk with new bitrate.
    print("Exporting {0}.ogg.".format(i+1))
    chunk.export(
        "app/src/main/assets/{0}/{1}.ogg".format(argv[1], i+1),
        bitrate = "128k",
        format = "ogg"
    )
