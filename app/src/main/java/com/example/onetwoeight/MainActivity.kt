package com.example.onetwoeight

import android.content.res.AssetFileDescriptor
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.annotation.RequiresApi
import java.util.*

class MainActivity : AppCompatActivity() {
    private val voices = listOf("ulrik", "cecilia", "emma", "asa", "pelle", "molly")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun resourceForButton(btn: Button): AssetFileDescriptor {
        val voice = voices[Random().nextInt(voices.size)]

        return baseContext.assets.openFd(voice + "/" + btn.text + ".ogg")
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun onClickBtn(view: View) {
        val mediaPlayer = MediaPlayer()
        mediaPlayer.setDataSource(resourceForButton(view as Button))
        mediaPlayer.prepare()
        mediaPlayer.start()
        mediaPlayer.setOnCompletionListener { mediaPlayer.release(); }
    }
}
